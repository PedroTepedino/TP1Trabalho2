#ifndef DATAHANDLER_H_
#define DATAHANDLER_H_

#include "pessoa.h"
#include <unordered_map>

class DataHandler
{
private :
    static DataHandler *shared_instance;
    unordered_map<string,Aluno*> alunos;
    unordered_map<string,Professor*> professores;
    unordered_map<string,Terceirizado*> terceirizados;
    unordered_map<string,Outros*> outros;
    vector<string>idAlunos;
    vector<string>idProfessores;
    vector<string>idTerceirizados;
    vector<string>idOutros;
    inline void addIdOutros(string id){idOutros.push_back(id);};
    inline void addIdProfessores(string id){idProfessores.push_back(id);};
    inline void addIdAlunos(string id){idAlunos.push_back(id);};
    inline void addIdTerceirizados(string id){idTerceirizados.push_back(id);};
public :
    /**
     * @brief Adiciona um aluno ao vetor de alunos; 
     */
    void addAluno(Aluno* a);
    /**
     * @brief Adiciona um preofessor ao vetor de professores; 
     */
    void addProfessor(Professor* p);
    /**
     * @brief Adiciona um terceirizado ao vetor de terceirizados;  
     */
    void addTerceirizado(Terceirizado* t);
    /**
     * @brief Adiciona um outro ao vetor de outros;  
     */
    void addOutros(Outros* o);
    /// conta quantas imagens o usuario com a id tem;
    int contaImagens(string id);
    /// Retorna a categoria da classe com o id fornecido 0 caso a id nao esteja cadastrado;
    int idExists(string id);
    /// Retorna o curso do aluno com a id fornecida;
    inline string getCurso(string idAluno){ return alunos[idAluno]->getCurso();};


    /// Rertorna o singleton de DataHandler;
    static DataHandler *instance();
    /**
    * @brief salva todas as instancias de pessoas armazenadas;
    */
    void saveAll();
    /**
    * @brief tenta carregar todas as instancias de pessoas;
    */
    void loadAll();
};

#endif