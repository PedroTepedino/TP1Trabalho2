#ifndef WINDOWREGIST_H_
#define WINDOWREGIST_H_

#include <QMainWindow>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QWidget>
#include <QVBoxLayout>
#include <QCheckBox>
#include <vector>

#include "pessoa.h"
#include "datahandler.h"


class RegistWindow : public QMainWindow
{
	Q_OBJECT

public :
	/**
	 * @brief Construtor da Classe ;
	 * 
	 * @param cargo Parametro que indica a classe do usuário a ser registrado,
	 * 				1 para Aluno, 2 para Professor, 3 para Terceizados e 4 para Outros;  
	 */
	explicit RegistWindow(QWidget *parent = 0, int cargo = 0) ;
	// cargo, 1 = Aluno, 2 = Professor, 3 = Terceirizados, 4 = Outros
	~RegistWindow() ;

	Aluno *aluno ;
	Professor *professor ;
	Terceirizado *terceirizado ;
	Outros *outro ;
	Pessoa *pessoa ;
	Materia *materia ;


private slots :
	/// Evento que é acioanado quando a página é fechada, nesse caso é usado para mostrar a mainwindow
	void closeEvent(QCloseEvent *closeEvent) ;
	/// Evento que é acionado quando o botaoContinuarMaterias é solto, que fecha a janela
	void eventContinuarMaterias() ;
	/// Evento que é acionado quando o botaoContinuar é solto, escondendo a janela atuala e mostrando a de escolha dos horários
	void eventContinuar() ;


private :
	QLineEdit *linhaNome ; 		//< Caixa de Edição de Texto que armazena o nome do novo usuário.
	QLineEdit *linhaSobrenome ;	//< Caixa de Edição de Texto que armazena o sobrenome do novo usuário.
	QLineEdit *linhaIdent ;		//< Caixa de Edição de Texto que armazena a identificação do novo usuário.
	QLineEdit *linhaAux ; 		//< Caixa de Edição de Texto que armazena o curso ou a função do novo usuário.

	QLabel *labelNome ;			//< Texto indicativo.
	QLabel *labelSobrenome ;	//< Texto indicativo.
	QLabel *labelMatricula ;	//< Texto indicativo.
	QLabel *labelCPF ;			//< Texto indicativo.
	QLabel *labelCurso ;		//< Texto indicativo.
	QLabel *labelFuncao ;		//< Texto indicativo.

	QVBoxLayout *layoutV ;				//< Layout que armazena as informações da janela principal.
	QVBoxLayout *layoutVMateriasLab ;	//< Layout que armazena o botão de Continuar da janela de seleção de horários.
	QVBoxLayout *layoutVMateriasSeg ;	//< Layout que armazena os Checkboxes da dos horários de Segunda.
	QVBoxLayout *layoutVMateriasTer ;	//< Layout que armazena os Checkboxes da dos horários de Terça.
	QVBoxLayout *layoutVMateriasQua ;	//< Layout que armazena os Checkboxes da dos horários de Quarta.
	QVBoxLayout *layoutVMateriasQui	;	//< Layout que armazena os Checkboxes da dos horários de Quinta.
	QVBoxLayout *layoutVMateriasSex	;	//< Layout que armazena os Checkboxes da dos horários de Sexta.

	QHBoxLayout *layoutHMaterias ;	//< Layout que armazena os Layouts dos horários dos dias das semana. 

	QWidget *widget ;		//< Widget Auxiliar que armazena o layout da pagina principal de Registro.
	QWidget *materias ;		//< Widget que armazena a janela de selação de horários.

	QPushButton *botaoContinuar ;			//< Botão que aciona o evento de continuar para a seleção.
	QPushButton *botaoContinuarMaterias ;	//< Botão que aciona o evento que termina o registro de um usuario.

	std::vector<QCheckBox *> botaoSeg ;	//< Vetor de Checkboxes dos horários de Segunda.
	std::vector<QCheckBox *> botaoTer ; //< Vetor de Checkboxes dos horários de Terça.
	std::vector<QCheckBox *> botaoQua ; //< Vetor de Checkboxes dos horários de Quarta.
	std::vector<QCheckBox *> botaoQui ; //< Vetor de Checkboxes dos horários de Quinta.
	std::vector<QCheckBox *> botaoSex ; //< Vetor de Checkboxes dos horários de Sexta.
	
	int cargo ; //< Variavel que armazena a classe do novo Usuário.
} ;

#endif