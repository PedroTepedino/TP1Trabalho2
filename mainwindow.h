#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <QMainWindow>
#include <QPushButton>
#include <QWidget>
#include <QRadioButton>
#include <QVBoxLayout>
#include <vector>
#include <QLineEdit>
#include <QLabel>

#include "pessoa.h"
#include "datahandler.h"
#include "opencvhandler.h"


class MainWindow : public QMainWindow
{
	Q_OBJECT 	

public :
	explicit MainWindow(QWidget *parent = 0) ;
	~MainWindow() ;

private slots :
	/// Método que abre a página de Registro quando o botaoRegigistrar é solto.
	void sinalRegistrar() ;
	/// Método que abre a página de Reconhecer quando o botatoRecohecer é solto.
	void sinalReconhecer() ;
	/// Método que abre a página de Registro de usuário, após a seleção da classe. 
	void sinalContinuarRegistrar() ;
	/// Método que salva todas as informações sobre os usuários, quando o botaoSair é precionado.
	void sinalSairSalvar() ;
	///	Método que abre a janela que ira tirar a foto de um usuário
	void sinalFoto() ;
	/// Método que salva a foto de um usuário
	void sinalOk() ;

private :
	QPushButton *botaoRegistrar ; 			//< Botão que leva à página de Registrar um novo usuário
	QPushButton *botaoReconhecer ;			//< Botão que leva à página de Reconhecer um usuário
	QPushButton *botaoContinuarRegistrar ;	//< Botão que leva à página de Registro, após escolher a classe do usuário
	QPushButton *botaoSair ;				//< Botão que salva os dados e sai do programa
	QPushButton *botaoFoto ;				//< Botão que salva uma foto para um usuário.
	QPushButton *botaoOk ;					//< Botão que salva uma foto para um usuário.	

	QWidget *window ;	//< Janela Auxiliar para permitir o layout da pagina
	QWidget *widget ;	//< Janela que armazena o layout de seleção da classe de um novo usuário
	QWidget *foto ;		//< Janela que amazena os trem pra tirar foto

	QVBoxLayout *layoutVertical ;	//< Armazena o layout dos objetos da página principal
	QVBoxLayout *layoutBotoes ;		//< Armazena o layout da página de seleção da classe do usuário
	QVBoxLayout *layoutFoto ;		//< Armazena os bagui doido das foto

	QRadioButton *botaoAluno ;		//< Botão para selecionar a classe Aluno
	QRadioButton *botaoProff ;		//< Botão para selecionar a classe Professor
	QRadioButton *botaoTerc	 ;		//< Botão para selecionar a classe Terceirizado
	QRadioButton *botaoOutro ;		//< Botão para selecionar a classe Outros

	QLineEdit *linhaFoto ;			//< Recebe a identificação de quem será tirado a foto

	QLabel *labelFoto ;

	OpenCVHandler *open ;

} ;

#endif