#include "windowrecog.h"
#include <string>

RecogWindow::RecogWindow(QWidget *parent) :
	QMainWindow(parent)
{
	widget = new QWidget(this) ;
	aviso = new QWidget() ;

	layoutVertical = new QVBoxLayout() ;
	layoutVAviso = new QVBoxLayout() ;

	botaoReconhecer = new QPushButton("Reconhecer", this) ;
	botaoOk = new QPushButton("OK") ;

	labelIdentificacao = new QLabel("Matricula/CPF") ;
	linhaIdentificacao = new QLineEdit() ;

	labelAceito = new QLabel("Aceito") ;
	labelRejeitado = new QLabel("Rejeitado") ;

	layoutVertical->addWidget(labelIdentificacao) ;
	layoutVertical->addWidget(linhaIdentificacao) ;
	layoutVertical->addWidget(botaoReconhecer) ;

	widget->setLayout(layoutVertical) ;
	
	this->setCentralWidget(widget) ;

	connect(botaoReconhecer, SIGNAL (released()), this, SLOT (recogEvent())) ;
	connect(botaoOk, SIGNAL (released()), this, SLOT (okEvent())) ;
}

RecogWindow::~RecogWindow()
{
	delete widget ;
	delete layoutVertical ;
	delete botaoReconhecer ;
	delete labelIdentificacao ;
	delete linhaIdentificacao ;
}

void RecogWindow::closeEvent(QCloseEvent *event)
{
	parentWidget()->show() ;
}

void RecogWindow::recogEvent()
{
	int cargo = DataHandler::instance()->idExists(linhaIdentificacao->text().toStdString()) ;
	std::string curso ;

	if (cargo == 2 || cargo == 3)
	{
		layoutVAviso->addWidget(labelAceito) ;
	}
	else if (cargo == 1)
	{
		curso = DataHandler::instance()->getCurso(linhaIdentificacao->text().toStdString()) ;
		if (curso.compare("CIC"))
		{
			layoutVAviso->addWidget(labelAceito) ;
		}
		else
		{
			layoutVAviso->addWidget(labelRejeitado) ;
		}
	}
	else 
	{
		layoutVAviso->addWidget(labelRejeitado) ;
	}
	layoutVAviso->addWidget(botaoOk) ;

	aviso->setLayout(layoutVAviso) ;

	aviso->show() ;
}

void RecogWindow::okEvent()
{
	delete aviso ;
	this->close() ;
}