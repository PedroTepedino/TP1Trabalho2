#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv/cv.hpp"

#include <string>
using namespace std;
using namespace cv;
using namespace boost;

class OpenCVHandler 
{
private:


/** Function Headers */
private:
    /** @brief detecta rostos e pode salvar fotos com o id recebido 
     * @param frame matriz capturada da camera;
     * @param ident string do identificador do usuario;
    */
    void detectAndDisplay( Mat frame, string ident);
    


public:
    /** @brief 
     *
     * @param id identificador do usuario a ser reconhecido;
    */
    void recognize(string id);

};
