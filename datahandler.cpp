#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv/cv.hpp"

#include <boost/format.hpp>

#include "datahandler.h"
#include <sys/stat.h>

DataHandler* DataHandler::shared_instance = 0;

inline bool exists(const string& name){
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

/*Classe DataHandler*/

#define USER_LIST "cadastrados.json"

DataHandler* DataHandler::instance(){
    if (!DataHandler::shared_instance){
        DataHandler::shared_instance = new DataHandler();
    }
    return DataHandler::shared_instance;
}

void DataHandler::addAluno(Aluno *a){
    alunos[a->getIdent()] = a;
    addIdAlunos(a->getIdent());
}

void DataHandler::addProfessor(Professor *p){
    professores[p->getIdent()] = p;
    addIdProfessores(p->getIdent());
}

void DataHandler::addTerceirizado(Terceirizado *t){
    terceirizados[t->getIdent()] = t;
    addIdTerceirizados(t->getIdent());
}

void DataHandler::addOutros(Outros *o){
    outros[o->getIdent()] = o;
    addIdOutros(o->getIdent());
}

int DataHandler::contaImagens(string id){
    string img;
    int i = 0;
    string jpg(".jpg" );
    img = (boost::format("%s%i%s") %id%i%jpg).str();
    while(exists(img)){
        i++;
        img = (boost::format("%s%i%s") %id%i%jpg).str();
    }
    return i;
}

int DataHandler::idExists(string id){
    if(alunos[id]){ return 1; }
    if(professores[id]) { return 2; } 
    if(terceirizados[id]) { return 3; }
    if(outros[id]) {return 4; }
    return 0;
}

void DataHandler::saveAll(){
    boost::property_tree::ptree idents;
    for (auto id : idAlunos){
        Aluno *p = alunos[id];
        string out = p->getIdent();
        out = out + ".json";
        idents.put(p->getIdent(), out);
        p->save();
    }
    for (auto id : idProfessores){
        Professor *p = professores[id];
        string out = p->getIdent();
        out = out + ".json";
        idents.put(p->getIdent(), out);
        p->save();
    }
    for (auto id: idTerceirizados){
        Terceirizado *p = terceirizados[id];
        string out = p->getIdent();
        out = out + ".json";
        idents.put(p->getIdent(), out);
        p->save();
    }
    for (auto id : idOutros){
        Outros *p = outros[id];
        string out = p->getIdent();
        out = out + ".json";
        idents.put(p->getIdent(), out);
        p->save();
    }
    boost::property_tree::write_json(USER_LIST, idents);
}

void DataHandler::loadAll(){
    if (exists(USER_LIST)){
        boost::property_tree::ptree users;
        boost::property_tree::read_json(USER_LIST, users);
        for (auto key : users){
            boost::property_tree::ptree user;
            boost::property_tree::read_json(users.get<string>(key.first), user);
            int classe = user.get<int>("Classe");
            // aluno
            if (classe == 0){
                Aluno *a = new Aluno(&user);
                addAluno(a);
            // Professor
            } else if (classe == 1){
                Professor *prof = new Professor(&user);
                addProfessor(prof);
            // Terceirizado
            }
            else if (classe == 2){
                Terceirizado *t = new Terceirizado(&user);
                addTerceirizado(t);

            // Outros
            } else if (classe == 3){
                Outros *o = new Outros(&user);
                addOutros(o);
            }
        }
    }
}