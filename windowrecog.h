#ifndef WINDOWRECOG_H_
#define WINDOWRECOG_H_

#include <QWidget>
#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>

#include "pessoa.h"
#include "datahandler.h"


class RecogWindow : public QMainWindow
{
	Q_OBJECT

public :
	explicit RecogWindow(QWidget *parent = 0) ;
	~RecogWindow() ;

private slots :
	/// Evento que é acionado quando a janela principal é fechada
	void closeEvent(QCloseEvent *event) ;
	/// Evento que é acionado quando o botaoReconhecer é solto
	void recogEvent() ;
	///	OK EVENT 
	void okEvent() ;

private :
	QPushButton *botaoReconhecer ;	//< Botao que aciona o evento de reconhcecimento facial.
	QPushButton *botaoOk ;			//< Botao que volta para a janela de reconhecimento.

	QLabel *labelIdentificacao ;	//<	Texto Indicativo. 
	QLabel *labelAceito ;			//< Texto Indicativo.
	QLabel *labelRejeitado ;		//< Texto Indicativo.

	QLineEdit *linhaIdentificacao ;	//< Caixa de Edição de Texto que Recebe a Identificacção do usuário a ser Reconhecido.

	QWidget *widget ;				//< Widget Auxiliar que recebe o Layout da página principal.
	QWidget *aviso ;				//< Widget que mostra se o usuário está ou nao permitido de entrar no linf.

	QVBoxLayout *layoutVertical ;	//< Layout que contem so objétos da janéla principal.
	QVBoxLayout *layoutVAviso ;
} ;

#endif