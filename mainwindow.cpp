#include "mainwindow.h"
#include "windowregist.h"
#include "windowrecog.h"

#include <QVBoxLayout>
#include <QApplication>
#include <QRadioButton>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	this->setMinimumHeight(250) ;
	this->setMinimumWidth(500) ;

	widget = new QWidget(this) ;
	window = new QWidget() ;
	foto = new QWidget() ;

	layoutVertical = new QVBoxLayout() ;
	layoutBotoes = new QVBoxLayout() ;
	layoutFoto = new QVBoxLayout() ;

	botaoRegistrar = new QPushButton("Registrar", this) ;
	botaoReconhecer = new QPushButton("Reconhecer", this) ;
	botaoContinuarRegistrar = new QPushButton("Continuar", this) ;
	botaoSair = new QPushButton("Sair", this) ;
	botaoFoto = new QPushButton("Tirar Foto", this) ;
	botaoOk = new QPushButton("OK", this) ;

	botaoAluno = new QRadioButton("Aluno", this) ;
	botaoProff = new QRadioButton("Professor", this) ;
	botaoTerc  = new QRadioButton("Terceirizado", this) ;
	botaoOutro = new QRadioButton("Outros", this) ;

	linhaFoto = new QLineEdit() ;

	labelFoto = new QLabel("Insira uma identificação") ;

	layoutVertical->addWidget(botaoRegistrar) ; 
	layoutVertical->addWidget(botaoReconhecer) ;
	layoutVertical->addWidget(botaoFoto) ;
	layoutVertical->addWidget(botaoSair) ;

	layoutBotoes->addWidget(botaoAluno) ;
	layoutBotoes->addWidget(botaoProff) ;
	layoutBotoes->addWidget(botaoTerc)  ;
	layoutBotoes->addWidget(botaoOutro)	;
	layoutBotoes->addWidget(botaoContinuarRegistrar) ;

	window->setLayout(layoutBotoes) ;
	

	layoutFoto->addWidget(labelFoto) ;
	layoutFoto->addWidget(linhaFoto) ;
	layoutFoto->addWidget(botaoOk) ;

	foto->setLayout(layoutFoto) ;


	widget->setLayout(layoutVertical) ;
	this->setCentralWidget(widget) ;

	connect(botaoRegistrar, SIGNAL (released()), this, SLOT (sinalRegistrar())) ;
	connect(botaoReconhecer, SIGNAL (released()), this, SLOT (sinalReconhecer())) ;
	connect(botaoContinuarRegistrar, SIGNAL (released()), this, SLOT (sinalContinuarRegistrar())) ;
	connect(botaoSair, SIGNAL (released()), qApp, SLOT (quit())) ;
	connect(botaoSair, SIGNAL (pressed()), this, SLOT (sinalSairSalvar())) ;
	connect(botaoFoto, SIGNAL (released()), this, SLOT (sinalFoto())) ;
	connect(botaoOk, SIGNAL (released()), this, SLOT (sinalOk())) ;
}

MainWindow::~MainWindow()
{
	delete botaoRegistrar ;
	delete botaoReconhecer ;
	delete botaoContinuarRegistrar ;
	
	delete widget ;

	delete botaoAluno ;
	delete botaoProff ;
	delete botaoTerc  ;
	delete botaoOutro ;
}

void MainWindow::sinalRegistrar()
{
	this->hide() ;
	window->show() ;
}

void MainWindow::sinalContinuarRegistrar()
{
	int i = 0 ; // Aluno = 1, Professor = 2, Terceirizado = 3, Outros = 4

	window->close() ;

	if (this->botaoAluno->isChecked())
		i = 1 ;
	else if (this->botaoProff->isChecked())
		i = 2 ;
	else if (this->botaoTerc->isChecked())
		i = 3 ;
	else if (this->botaoOutro->isChecked())
		i = 4 ;

	if (i == 0)
	{
		this->show() ;
	}
	else
	{
		RegistWindow *regist = new RegistWindow(this, i) ;
		regist->show() ;
	}
}

void MainWindow::sinalReconhecer()
{
	this->hide() ;
	RecogWindow *recog = new RecogWindow(this) ;
	recog->show() ;
}

void MainWindow::sinalSairSalvar()
{
	DataHandler::instance()->saveAll() ;
} 

void MainWindow::sinalFoto()
{
	
	
	foto->show() ;
	
}

void MainWindow::sinalOk() 
{
	open = new OpenCVHandler() ;
	open->recognize(linhaFoto->text().toStdString()) ;
	foto->hide() ;
	delete open;
}