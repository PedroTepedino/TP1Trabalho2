#ifndef PESSOA_H_
#define PESSOA_H_

#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"

// enum dias {segunda = 1, terca, quarta, quinta, sexta, sabado, domingo} ;
// enum horarios {oito = 1, dez, doze, quatorze, dezesseis, dezoito, vinte, vintedois} ;
using namespace std;

class Horario
{
private :
    /// Hora de inicio;
    int horario ;
    /// Dia;
    int dia ;
    // int diaMes ;
    // int lab;
public :
    // Horario(int horario, int dia, int diaMes, int lab) ;
    /** @brief Cria um Horario
    *
    *   @param horario, a hora de inicio da aula/reserva;
    *   @param dia, dia da semana que acontece a aula/reserva;
    */
    Horario(int horario, int dia) ;
    ~Horario() ;
    int getHorario() ;
    int getDiaSemana() ;
    // int getDiaMes() ;
    // int getLab() ;
    bool operator== (Horario h) ;
};

class Materia
{
private :
    /// identificador da matéria
    string ident ;
    /// horarios nos quais a matéria acontece;
    vector<Horario> horarios ;
public :
    /// Cria uma matéria com o identificador;
    Materia(string ident) ;
    inline string getIdent(){return ident;};
    /// Adiciona um Horario à materia;
    inline void addHorario(Horario h){this->horarios.push_back(h);};
    inline vector<Horario> getHorarios(){return horarios;};

    ~Materia() ;
};

class Pessoa
{
protected :

    /// Identificador unico da Pessoa (cpf, matricula, identidade...);
    string ident ;
    /// Nome da Pessoa;
    string nome ;
    /// Sobrenome da Pessoa
    string sobrenome ;
    // classe foto lembra dessa porra !!!!!! caralho
public :
    inline string getIdent(){return ident;};
    inline string getNome(){return nome;};
    inline string getSobrenome(){return sobrenome;};
    /** @brief Cria uma pessoa;
    *
    *   @param ident é um identificador unico da pessoa
    *   @param nome é o nome da pessoa;
    *   @param sobrenome é o nome da pessoa;
    */
    Pessoa(string ident, string nome, string sobrenome) ;
    Pessoa(){};
    /** @brief converte os dados de uma Pessoa em um dicionario ptree.
    *
    *   @return ptree com os dados da pessoa;
    */
    boost::property_tree::ptree* save() ;
    ~Pessoa() ;


};

class Aluno : public Pessoa
{
private :
    /// Matérias que o aluno participa;
    vector<Materia> materias ;
    /// Curso do aluno;
    string curso ;
public :
    inline string getCurso(){return curso;} ;
    /** @brief Cria um Aluno;
    *
    *   @param p é a pessoa que o aluno representa
    *   @param curso é o curso do aluno;
    */
    Aluno(Pessoa p, string curso) ;
    /** @brief Cria um Aluno a partir de um dicionario recuperado de um json;
    *
    *   @param dicionario boost::property_tree::ptree onde os dados do aluno estão;
    */
    Aluno(boost::property_tree::ptree* dic) ;
    ~Aluno() ;
    /** @brief Adiciona uma matéria ao vetor de matérias do Aluno;
    *
    *   @param matéria a ser adicionada;
    */
    int addMateria(Materia m) ;
    /** @brief Remove uma matéria do vetor de matérias do Aluno;
    *
    *   @param matéria a ser adicionada;
    */
    int reMateria(int m) ;
    /** @brief converte os dados do Aluno em um dicionario ptree e o salva em formato "'this->ident'.json".
    *
    *   @return ptree com os dados do Aluno;
    */
    boost::property_tree::ptree* save() ;
};

class Professor : public Pessoa
{
private :
    /// Vetor de matérias que o professor ministra.
    vector<Materia> materias ;
    // vector<Horario> horarios ;
public :
    Professor(Pessoa p) ;
    /** @brief Cria um Professor a partir de um dicionario recuperado de um json;
    *
    *   @param dicionario boost::property_tree::ptree onde os dados do Professor estão;
    */
    Professor(boost::property_tree::ptree* dic) ;
    ~Professor() ;
    /** @brief Adiciona uma matéria ao vetor de matérias ministradas do professor;
    *
    *   @param matéria a ser adicionada;
    */
    int addMateria(Materia m) ;
    /** @brief Remove uma matéria do vetor de matérias ministradas do professor;
    *
    *   @param indice da matéria a ser removida;
    */
    int reMateria(int m) ;
    // int reservarHorario(Horario h) ;
    // int removerHorario(int h) ;
    /** @brief converte os dados do Professor em um dicionario ptree e o salva em formato  "'this->ident'.json".
    *
    *   @return ptree com os dados do Professor;
    */
    boost::property_tree::ptree* save() ;
};

class Terceirizado : public Pessoa
{
private :
    string funcao ;     //< funcao do funcionario
public :
    /** @brief Cria um Terceirizado;
    *
    *   @param p é a pessoa que contém as informações basicas do Terceirizado;
    *   @param funcao que o Terceirizado desempenhas
    */
    Terceirizado(Pessoa p, string funcao) ;
    /** @brief Cria um Terceirizado;
    *
    *   @param dic é o dicionario r
    */
    Terceirizado(boost::property_tree::ptree* dic) ;
    ~Terceirizado() ;
    /** @brief Cria um Terceirizado a partir de um dicionario recuperado de um json;
    *
    *   @param dicionario boost::property_tree::ptree onde os dados do Outros estão;
    */
    boost::property_tree::ptree* save() ;
};

class Outros : public Pessoa
{
private :
    vector<Horario> horarios ;
public :
    /** @brief Cria um Outros;
    *
    *   @param p é a pessoa que contém os dados basicos do Outros;
    */
    Outros(Pessoa p) ;
    /** @brief Cria um Outros a partir de um dicionario recuperado de um json;
    *
    *   @param dicionario boost::property_tree::ptree onde os dados do Outros estão;
    */
    Outros(boost::property_tree::ptree* dic) ;
    ~Outros() ;
    // int reservarHorario(Horario h) ;
    // int removerHorario(int h) ;
    /** @brief converte os dados de Outros em um dicionario ptree e o salva em formato "'this->ident'.json".
    *
    *   @return ptree com os dados de Outros;
    */
    boost::property_tree::ptree* save() ;
};


#endif
