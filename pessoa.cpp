#include <vector>
#include <string>

#include "pessoa.h"
#include "boost/format.hpp"
#include "boost/property_tree/ptree.hpp"


/*Classe Pessoa*/
Pessoa::Pessoa(std::string ident, std::string nome, std::string sobrenome)
    : ident(ident), nome(nome), sobrenome(sobrenome)
{

}

Pessoa::~Pessoa()
{

}

boost::property_tree::ptree* Pessoa::save(){
    boost::property_tree::ptree* dic = new boost::property_tree::ptree;
    dic->put("Ident", ident);
    dic->put("Nome", nome);
    dic->put("Sobrenome", sobrenome);
    return dic;
}


/*Classe Aluno*/
Aluno::Aluno(Pessoa p, std::string curso)
    : Pessoa(p.getIdent(), p.getNome(), p.getSobrenome()) , curso(curso)
{

}


Aluno::~Aluno()
{

}
Aluno::Aluno(boost::property_tree::ptree *dic){
    this->ident = dic->get<string>("Ident");;
    this->nome = dic->get<string>("Nome");;
    this->sobrenome = dic->get<string>("Sobrenome");
    this->curso = dic->get<string>("Curso");
    int materias = dic->get<int>("ContadorMaterias");
    for (int i = 0 ; i < materias; i++){
        string chave("Materias.");
        chave = (boost::format("%s%i") %chave%i).str();
        string chaveHorarios = chave + "ContagemHorarios";
        int horarios = dic->get<int>(chaveHorarios);
        vector<Horario> vh;
        for (int j = 0; j < horarios ; j++){
            string hora = (boost::format("%s%i") %chave%j).str();
            string horario = hora + ".horario";
            string dias = hora + ".dias";
            // string diames = hora + ".diames";
            // string lab = hora+ ".lab";
            int nhorario = dic->get<int>(horario);
            int dsemana = dic->get<int>(dias);
            // int dmes = dic->get<int>(diames);
            // int nlab = dic->get<int>(lab);

            // Horario h(nhorario,dsemana, dmes, nlab);
            Horario h(nhorario,dsemana);
            vh.push_back(h);
        }
        string ident = dic->get<string>(chave);
        if (vh.size()) {
            Materia m(ident);
            this->addMateria(m);
        }

    }
}

int Aluno::addMateria(Materia m)
{
    this -> materias.push_back(m) ;
    return this -> materias.size() ;
}

int Aluno::reMateria(int m)
{
    this -> materias.erase(this->materias.begin() + m - 1) ;
    return this -> materias.size() ;
}

boost::property_tree::ptree* Aluno::save(){
    boost::property_tree::ptree* dic = Pessoa::save();
    dic->put("Curso", curso);
    dic->put("Classe", 0);
    dic->put("ContadorMaterias", materias.size());
    // Para cada materia adicionar uma chave para a materia do indice i;
    for (int i = 0 ; i < materias.size(); i++){
        // formatando via boost a chave "Materia.'contador'""
        string str("Materias.");
        str = (boost::format("%s%i") %str%i).str();
        // Adicionando materia.i
        dic->put(str, materias[i].getIdent());
        int horarios = materias[i].getHorarios().size();

        string contHorarios = str + "ContagemHorarios";
        dic->put(contHorarios, horarios);

        for (int j = 0; j < horarios; j++){
            Horario h = materias[i].getHorarios()[j];
            string hora = (boost::format("%s%i") %str%j).str();
            string horario = hora + ".horario";
            string dias = hora + ".dias";
            // string diames = hora + ".diames";
            // string lab = hora+ ".lab";
            dic->put(horario, h.getHorario());
            dic->put(dias, h.getDiaSemana());
            // dic->put(diames, h.getDiaMes());
            // dic->put(lab, h.getLab());
        }
    }

    // Formatando identidade.json
    string filename = getIdent();
    filename = filename + ".json";
    boost::property_tree::write_json(filename, *dic);

    return dic;
}


/*Classe Professor*/
Professor::Professor(Pessoa p)
    : Pessoa(p.getIdent(), p.getNome(), p.getSobrenome())
{

}
Professor::Professor(boost::property_tree::ptree *dic){
    this->ident = dic->get<string>("Ident");;
    this->nome = dic->get<string>("Nome");;
    this->sobrenome = dic->get<string>("Sobrenome");
    int materias = dic->get<int>("ContadorMaterias");
    for (int i = 0 ; i < materias; i++){
        string chave("Materias.");
        chave = (boost::format("%s%i") %chave%i).str();
        string chaveHorarios = chave + "ContagemHorarios";
        int horarios = dic->get<int>(chaveHorarios);
        vector<Horario> vh;
        for (int j = 0; j < horarios ; j++){
            string hora = (boost::format("%s%i") %chave%j).str();
            string horario = hora + ".horario";
            string dias = hora + ".dias";
            // string diames = hora + ".diames";
            // string lab = hora+ ".lab";
            int nhorario = dic->get<int>(horario);
            int dsemana = dic->get<int>(dias);
            // int dmes = dic->get<int>(diames);
            // int nlab = dic->get<int>(lab);

            // Horario h(nhorario,dsemana, dmes, nlab);
            Horario h(nhorario,dsemana);
            vh.push_back(h);
        }
        string ident = dic->get<string>(chave);
        if (vh.size()) {
            Materia m(ident);
            this->addMateria(m);
        }

    }
}

Professor::~Professor()
{

}

int Professor::addMateria(Materia m)
{
    this -> materias.push_back(m) ;
    return this -> materias.size() ;
}

int Professor::reMateria(int m)
{
    this -> materias.erase(this->materias.begin() + m - 1) ;
    return this -> materias.size() ;
}

// int Professor::reservarHorario(Horario h)
// {
//     this -> horarios.push_back(h) ;
//     return this -> horarios.size() ;
// }
//
//
// int Professor::removerHorario(int h)
// {
//     this -> horarios.erase(this->horarios.begin() + h - 1) ;
//     return this -> horarios.size() ;
// }

boost::property_tree::ptree* Professor::save() {
    boost::property_tree::ptree* dic = Pessoa::save();

    // Adicionar chave para recuperar a classe desse usuario;
    dic->put("Classe",1);
    // Adicionar contagem de materias cadastradas para esse professor;
    dic->put("ContadorMaterias", materias.size());
    // Para cada materia adicionar uma chave para a materia do indice i;
    for (int i = 0 ; i < materias.size(); i++){
        // formatando via boost a chave "Materia.'contador'""
        string str("Materias.");
        str = (boost::format("%s%i") %str%i).str();
        // Adicionando materia.i
        dic->put(str, materias[i].getIdent());
        int horarios = materias[i].getHorarios().size();

        string contHorarios = str + "ContagemHorarios";
        dic->put(contHorarios, horarios);

        for (int j = 0; j < horarios; j++){
            Horario h = materias[i].getHorarios()[j];
            string hora = (boost::format("%s%i") %str%j).str();
            string horario = hora + ".horario";
            string dias = hora + ".dias";
            // string diames = hora + ".diames";
            // string lab = hora+ ".lab";
            dic->put(horario, h.getHorario());
            dic->put(dias, h.getDiaSemana());
            // dic->put(diames, h.getDiaMes());
            // dic->put(lab, h.getLab());
        }
    }
    // Formatando identidade.json
    string filename = getIdent();
    filename = filename + ".json";
    boost::property_tree::write_json(filename, *dic);
    return dic;
}

/*******************************************/
/*Lembrar de Fazer a colizao entre reservas*/
/*******************************************/


/*Classe Terceirizado*/
Terceirizado::Terceirizado(Pessoa p, std::string funcao)
    : Pessoa(p.getIdent(), p.getNome(), p.getSobrenome()), funcao(funcao)
{

}

Terceirizado::Terceirizado(boost::property_tree::ptree *dic){
    this->ident = dic->get<string>("Ident");;
    this->nome = dic->get<string>("Nome");;
    this->sobrenome = dic->get<string>("Sobrenome");
    this->funcao = dic->get<string>("Funcao");
}


Terceirizado::~Terceirizado()
{

}

boost::property_tree::ptree* Terceirizado::save(){
    boost::property_tree::ptree* dic = Pessoa::save();
    dic->put("Classe", 2);
    dic->put("Funcao", funcao);
    // Formatando via boost chave identidade.json
    string filename = getIdent();
    filename = filename + ".json";
    boost::property_tree::write_json(filename, *dic);
    return dic;
}


/*Classe Outros*/
Outros::Outros(Pessoa p)
    : Pessoa(p.getIdent(), p.getNome(), p.getSobrenome())
{

}

Outros::Outros(boost::property_tree::ptree *dic){
    this->ident = dic->get<string>("Ident");;
    this->nome = dic->get<string>("Nome");;
    this->sobrenome = dic->get<string>("Sobrenome");
}

Outros::~Outros()
{

}

// int Outros::reservarHorario(Horario h)
// {
//     this -> horarios.push_back(h) ;
//     return this -> horarios.size() ;
// }
//
// int Outros::removerHorario(int h)
// {
//     this -> horarios.erase(this->horarios.begin() + h - 1) ;
//     return this -> horarios.size() ;
// }

boost::property_tree::ptree* Outros::save(){
    boost::property_tree::ptree* dic = Pessoa::save();
    dic->put("Classe", 3);
    // Formatando via boost chave identidade.json
    string filename = getIdent();
    filename = filename + ".json";
    boost::property_tree::write_json(filename, *dic);
    return dic;
}

/*Classe Horario*/
// Horario::Horario(int horario, int dia, int diaMes, int lab)
//     : horario(horario), dia(dia), diaMes(diaMes), lab(lab)
// {
//
// }
Horario::Horario(int horario, int dia)
    : horario(horario), dia(dia)
{

}

Horario::~Horario()
{

}

int Horario::getHorario()
{
    return this -> horario ;
}

int Horario::getDiaSemana()
{
    return this -> dia ;
}

// int Horario::getDiaMes()
// {
//     return this -> diaMes ;
// }

// int Horario::getLab()
// {
//     return this -> lab ;
// }

bool Horario::operator== (Horario h)
{
    if (this->horario == h.getHorario())
        if (this->dia == h.getDiaSemana())
            // if (this->diaMes == h.getDiaMes())
                // if (this->lab == h.getLab())
                    return true ;
    /*Caso alguma das condicoes seja falsa, retorna falso*/
    return false;
}

/*Classe Materia*/
Materia::Materia(std::string ident)
    : ident(ident)
{

}

Materia::~Materia()
{

}


