#include "mainwindow.h"
#include "datahandler.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	DataHandler::instance()->loadAll() ;
	QApplication app(argc, argv) ;

	MainWindow mainwindow ;
	mainwindow.show() ;

	return app.exec() ;
}	