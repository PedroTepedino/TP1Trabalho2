#include <iostream>
#include <vector>
#include "pessoa.cpp"
#include "boost/property_tree/json_parser.hpp"
#include "boost/property_tree/ptree.hpp"

#include "datahandler.cpp"

using namespace std;



int main(int argc, char *argv[])
{
    DataHandler* single = DataHandler::instance();
    single->loadAll();
    
    Pessoa p1("5678910", "nome", "sobrenome");
    Aluno *a = new Aluno(p1, "gabriel tepedino");

    // a.save();
    Pessoa p2("123456", "nome", "sobrenome");
    Professor *prof = new Professor(p2);
    // Horario(horarios horario, dias dia, int diaMes, int lab) ;
    Horario h1(1, 1);
    Horario h2(2, 3);
    Materia m("APC");
    m.addHorario(h1);
    m.addHorario(h2);
    prof->addMateria(m);

    // prof.save();
    // cout << prof->getIdent() << "  asdasda"  << endl;
    single->addProfessor(prof);
    single->addAluno(a);


    Pessoa p3("111", "nome", "sobrenome");
    Outros *outro = new Outros(p3);
    Pessoa p4("333", "nome", "sobrenome");
    Terceirizado *terc = new Terceirizado(p4,"barrar o Gabriel");
    Pessoa p5("444", "nome", "sobrenome");
    Terceirizado *terc2 = new Terceirizado(p5,"barrar o Pedro Tepedino");

    single->addOutros(outro);
    single->addTerceirizado(terc);
    single->addTerceirizado(terc2);

    single->saveAll();
	return 0;
}
