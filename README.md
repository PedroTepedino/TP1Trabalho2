# Trabalho 2 - Sistema de Entrada do Linf

### Proposta:
O Projeto consiste em um sistema que reconhece alguem ao tentar entrar no Linf, atravez de autenticação de inforações fornecidas e reconhecimento facial.
O projeto será feito na linguagem C++ e para o reconhecimento facial será utilizado a biblioteca OpenCV.

### macOS Instalação, máquina virtual (VirtualBox):

Um dos integrantes trabalhou usando um macbook pro 13' rodando o sistema operacional macOS Sierra Version executando uma máquina virtual com o sistema Ubunu 16.04, para a instalação a configuração da mesma, foram executados os seguintes passos:

* Download instalação do Software [Oracle VM VirtualBox](https://www.virtualbox.org/wiki/Downloads) Version 5.2.0 r118431;
* Download de uma iso de instalação do [Ubuntu 16.04](http://releases.ubuntu.com/16.04/)
* Criar uma partição virtual para a nossa VM com espaço de 100gb;
* Executar a VM com a iso do Ubuntu montada.

### macOS Compartilhando a webCam com a VM:

* Para compartilhar a webcam é necessário o download do pacote adicional "VirtualBox 5.2.0 Oracle VM VirtualBox Extension Pack", na mesma [pagina de download](https://www.virtualbox.org/wiki/Downloads) que o Software principal.
* Após instalar o pacote e reiniciar seu computador, abra o VirtualBox execute a VM que deseja permitir acesso a sua webcam.
* Abra uma janela do terminal no sistema Host (macOS) e execute os seguintes comandos
```
	~$ VBoxManage list webcams
	Video Input Devices: 1
	.1 "FaceTime HD Camera"
	CF115HP5HNTF6YU99
	~$ VBoxManage controlvm "Linux100g" webcam attach .1
```
### Bibliotecas Necessárias
Além das bibliotecas necessárias para executar codigos em c++11 são necessários as seguintes bibliotecas:
*

### Instalação (Ubuntu 16.04)


* Execute os passos disponiveis em https://github.com/girayaThiago/bash-opencv
* Em seguida o segunte comando:
```
~$ sudo apt-get install libboost-all-dev
```
* Esses 2 passos instalam todos os pacotes necessários para executar nosso projeto.
* Em seguida cria uma pasta chamada build, nela execute o comando ```cmake ..```
* em seguida execute o comando ```./Sistema```


### Instruções de Uso

##### 1. Registro.

![alt text](https://assets.gitlab-static.net/PedroTepedino/TP1Trabalho2/raw/master/fotos/passo1.png "Menu Principal")

###### 1.1 Clique em ```registrar```.

![alt text](https://assets.gitlab-static.net/PedroTepedino/TP1Trabalho2/raw/master/fotos/passo2.1.png "Selecao de Classe")

###### 1.2 Selecione Qual tipo de usuario irá cadastrar, e clique ```Continuar```.

![alt text](https://assets.gitlab-static.net/PedroTepedino/TP1Trabalho2/raw/master/fotos/passo4.png "Preenchendo dados")

###### 1.3 Preencha os dados do Usuario, e clique ```Continuar```.

![alt text](https://assets.gitlab-static.net/PedroTepedino/TP1Trabalho2/raw/master/fotos/passo6.png "Selecionando Horarios")

###### 1.4 Selecione os horarios nos dias que tem aula e clique ```Continuar```.

![alt text](https://assets.gitlab-static.net/PedroTepedino/TP1Trabalho2/raw/master/fotos/passo1.png "Menu Principal")

###### 1.5 Fim do Cadastro.


##### 2. Adicionando Fotos.

###### 2.1 Clique em ```Tirar Foto```

###### 2.2 Informe a Matricula.

###### 2.3 Aperte F para tirar Fotos.

###### 2.4 Aperte C para fechar a camera.

###### 2.5 Fim da operação;

##### 3. Reconhecimento.

###### 3.1 Clique em Reconhecer.

###### 3.2 Informe um identificador cadastrado.

###### 3.3 Se o usuario estiver cadastrado o reconhecimento facial permitirá a entrada. [TODO]


### Diagrama de Sequencia
![diagrama_de_sequencia](https://assets.gitlab-static.net/PedroTepedino/TP1Trabalho2/raw/master/Diagrama_de_Sequencia/diagrama_de_sequencia.png "diagrama de sequencia")

### [Documentação](https://gitlab.com/PedroTepedino/TP1Trabalho2/blob/master/doxygenFile/html/index.html)

Links interessantes sobre visão computacional:

https://youtu.be/mSFHKAvTGNk?t=1004

http://i.hilariousgifs.com/ghost-snapchat-dog-filter.gif
