#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv/cv.hpp"

#include <boost/format.hpp>

#include <iostream>
#include <stdio.h>
#include <string>

int contador = 0;
int cap = 0;
using namespace std;
using namespace cv;
using namespace boost;

/** Function Headers */
void detectAndDisplay( Mat frame );

/** Global variables */

String face_cascade_name = "haarcascade_frontalface_alt.xml";
String eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
string window_name = "Capture - Face detection";
RNG rng(12345);

/** @function main */
int main( int argc, const char** argv )
{
  CvCapture* capture;
  Mat frame;


  //-- 1. Load the cascades
  if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };
  if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading\n"); return -1; };

  //-- 2. Read the video stream
  capture = cvCaptureFromCAM( -1 );
  if( capture )
  {
  	
    while( true )
    {
      frame = cvQueryFrame( capture );
      //-- 3. Apply the classifier to the frame
      if( !frame.empty() )
      { detectAndDisplay( frame); }
      else
      { printf(" --(!) No captured frame -- Break!"); break; }

      int c = waitKey(10);
      if( (char)c == 'c' ) { break; }
      else if ((char)c == 'f'){
      	cap = 1;
      }
    }
  } else {
    cout << "no cam" << endl;
  }

  return 0;
}

/** @function detectAndDisplay */
void detectAndDisplay( Mat frame )
{
  std::vector<Rect> faces;
  Mat frame_gray;
  cvtColor( frame, frame_gray, CV_BGR2GRAY );
  equalizeHist( frame_gray, frame_gray );

  //-- Detect faces
  face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );

  for( size_t i = 0; i < faces.size(); i++ )
  {
    Point origin(faces[i].x, faces[i].y);
    Point end( faces[i].x + faces[i].width, faces[i].y + faces[i].height );
    if (cap == 1){
    	Mat crop;
    	string teste(".jpg");
      string ident("1234");
    	string foo = (boost::format("%s%i%s") %ident%contador%teste).str();
    	// string foo = "img.jpg";
    	frame(Rect(faces[i].x,faces[i].y,faces[i].width,faces[i].height)).copyTo(crop);
    	imwrite(foo, crop);
    	cap = 0;
    	contador++;

    }

    Mat faceROI = frame_gray( faces[i] );
    
    //std::vector<Rect> eyes;
    rectangle(frame, origin, end, Scalar(0,255,0));
  }
  //-- Show what you got
  flip(frame,frame,1);
  imshow( window_name, frame);
}
