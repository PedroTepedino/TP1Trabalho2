#include "windowregist.h"

#include <QMainWindow>
#include <QEventLoop>
#include <string>


RegistWindow::RegistWindow(QWidget *parent, int cargo) :
	QMainWindow(parent), cargo(cargo)
{
/* Alocando os objetos que serao usados na classe */
	linhaNome = new QLineEdit() ;
	linhaSobrenome = new QLineEdit() ;
	linhaIdent = new QLineEdit() ;
	linhaAux = new QLineEdit() ;

	labelNome = new QLabel("Nome") ;
	labelSobrenome = new QLabel("Sobrenome") ;
	labelMatricula = new QLabel("Matrícula") ;
	labelCPF = new QLabel("CPF") ;
	labelCurso = new QLabel("Curso") ;
	labelFuncao = new QLabel("Funcao") ;

	layoutV = new QVBoxLayout() ;
	layoutVMateriasLab = new QVBoxLayout() ;
	layoutVMateriasSeg = new QVBoxLayout() ;
	layoutVMateriasTer = new QVBoxLayout() ;
	layoutVMateriasQua = new QVBoxLayout() ;
	layoutVMateriasQui = new QVBoxLayout() ;
	layoutVMateriasSex = new QVBoxLayout() ;

	layoutHMaterias	= new QHBoxLayout() ;

	widget = new QWidget(this) ;
	materias = new QWidget() ;

	botaoContinuar = new QPushButton("Continuar") ;
	botaoContinuarMaterias = new QPushButton("continuar") ;

	botaoSeg.push_back(new QCheckBox("08 h")) ;
	botaoSeg.push_back(new QCheckBox("10 h")) ;
	botaoSeg.push_back(new QCheckBox("14 h")) ;
	botaoSeg.push_back(new QCheckBox("16 h")) ;
	botaoSeg.push_back(new QCheckBox("19 h")) ;
	botaoSeg.push_back(new QCheckBox("21 h")) ;

	botaoTer.push_back(new QCheckBox("08 h")) ;
	botaoTer.push_back(new QCheckBox("10 h")) ;
	botaoTer.push_back(new QCheckBox("14 h")) ;
	botaoTer.push_back(new QCheckBox("16 h")) ;
	botaoTer.push_back(new QCheckBox("19 h")) ;
	botaoTer.push_back(new QCheckBox("21 h")) ;

	botaoQua.push_back(new QCheckBox("08 h")) ;
	botaoQua.push_back(new QCheckBox("10 h")) ;
	botaoQua.push_back(new QCheckBox("14 h")) ;
	botaoQua.push_back(new QCheckBox("16 h")) ;
	botaoQua.push_back(new QCheckBox("19 h")) ;
	botaoQua.push_back(new QCheckBox("21 h")) ;

	botaoQui.push_back(new QCheckBox("08 h")) ;
	botaoQui.push_back(new QCheckBox("10 h")) ;
	botaoQui.push_back(new QCheckBox("14 h")) ;
	botaoQui.push_back(new QCheckBox("16 h")) ;
	botaoQui.push_back(new QCheckBox("19 h")) ;
	botaoQui.push_back(new QCheckBox("21 h")) ;

	botaoSex.push_back(new QCheckBox("08 h")) ;
	botaoSex.push_back(new QCheckBox("10 h")) ;
	botaoSex.push_back(new QCheckBox("14 h")) ;
	botaoSex.push_back(new QCheckBox("16 h")) ;
	botaoSex.push_back(new QCheckBox("19 h")) ;
	botaoSex.push_back(new QCheckBox("21 h")) ;

/* Fazendo o layout da pagina de Registro */
	layoutV->addWidget(labelNome);
	layoutV->addWidget(linhaNome) ;
	layoutV->addWidget(labelSobrenome) ;
	layoutV->addWidget(linhaSobrenome) ;	
	
	/* Conteudos da pagina dependem da classe que queremos alocar */
	if (cargo == 1 || cargo == 2)
		layoutV->addWidget(labelMatricula) ;
	else 
		layoutV->addWidget(labelCPF) ;

	layoutV->addWidget(linhaIdent) ;

	if (cargo == 1)
	{
		layoutV->addWidget(labelCurso) ;
		layoutV->addWidget(linhaAux) ;
	}
	else if (cargo == 3)
	{
		layoutV->addWidget(labelFuncao) ;
		layoutV->addWidget(linhaAux) ;
	}

	layoutV->addWidget(botaoContinuar) ;

	widget->setLayout(layoutV) ;
	this->setCentralWidget(widget) ;

	this->show() ; // mostra a tela principal de registro 

/* Fazendo o layout da pagina de registrar matereia  */

	for (int i = 0; i < 6 ; i++)
	{	
		layoutVMateriasSeg->addWidget(botaoSeg[i]) ;
		layoutVMateriasTer->addWidget(botaoTer[i]) ;
		layoutVMateriasQua->addWidget(botaoQua[i]) ;
		layoutVMateriasQui->addWidget(botaoQui[i]) ;
		layoutVMateriasSex->addWidget(botaoSex[i]) ;
	}

	layoutVMateriasLab->addWidget(botaoContinuarMaterias) ; //Chama lab, pq é aqui q ficaria a escolha do laboratorio
	
	layoutHMaterias->addLayout(layoutVMateriasLab) ;
	layoutHMaterias->addLayout(layoutVMateriasSeg) ;
	layoutHMaterias->addLayout(layoutVMateriasTer) ;
	layoutHMaterias->addLayout(layoutVMateriasQua) ;
	layoutHMaterias->addLayout(layoutVMateriasQui) ;
	layoutHMaterias->addLayout(layoutVMateriasSex) ;


	materias->setLayout(layoutHMaterias);



/* Conectando os sinais oas respectivos metodos */
	connect(botaoContinuar, SIGNAL (released()), this,  SLOT (eventContinuar())) ;
	connect(botaoContinuarMaterias, SIGNAL (released()), this, SLOT (eventContinuarMaterias())) ;
}

RegistWindow::~RegistWindow()
{
	/*delete linhaNome ;
	delete linhaSobrenome ;
	delete linhaIdent ;
	delete linhaAux ;

	delete labelNome ;
	delete labelSobrenome ;
	delete labelMatricula ;
	delete labelCPF ;
	delete labelCurso ;
	delete labelFuncao ;
	delete labelNumeroMaterias ;

	delete layoutV ;
	delete layoutVNumeroMaterias ;
	delete layoutVMaterias ;

	delete botaoContinuar ;
	delete botaoContinuarNumeroMaterias ;
	
	delete widget ;
	delete numeroMateria ;
	delete materias ;

	delete spinMaterias ; */
}

void RegistWindow::closeEvent(QCloseEvent *closeEvent)
{
	std::string nome  = linhaNome->text().toStdString() ;
	std::string sobrenome = linhaSobrenome->text().toStdString() ;
	std::string ident = linhaIdent->text().toStdString() ;
	std::string aux = linhaAux->text().toStdString() ;

	pessoa = new Pessoa(ident, nome, sobrenome) ;
	materia = new Materia("MATERIA") ;

	if (cargo == 1)
	{
		aluno = new Aluno(*pessoa, aux) ;
		aluno->addMateria(*materia) ;
		DataHandler::instance()->addAluno(aluno) ;
	}
	else if (cargo == 2)
	{
		professor = new Professor(*pessoa) ;
		professor->addMateria(*materia) ;
		DataHandler::instance()->addProfessor(professor) ;
	}
	else if (cargo == 3)
	{
		terceirizado = new Terceirizado(*pessoa, aux) ;
		DataHandler::instance()->addTerceirizado(terceirizado) ;
	}
	else
	{
		outro = new Outros(*pessoa) ;
		DataHandler::instance()->addOutros(outro) ;
	}
	
	parentWidget()->show() ;
}

void RegistWindow::eventContinuar()
{
	this->hide() ;
	materias->show() ;
}

void RegistWindow::eventContinuarMaterias()
{
	materias->hide() ;

	this->close() ;
}